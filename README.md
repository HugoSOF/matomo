## Matomo ? : 
Matomo, anciennement Piwik4 jusqu’en 2018, est un logiciel libre et open source de mesure de statistiques web, successeur de PhpMyVisites conçu pour être une alternative libre à Google Analytics5. Matomo fonctionne sur des serveurs web PHP/MySQL.
En juillet 2017, Matomo est utilisé par plus d'un million de sites web6 et crédite 1,3 % de parts de marché. 

Matomo est aujourd'hui traduit dans 54 langues et est régulièrement mis à jour par sa communauté.
